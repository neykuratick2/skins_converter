from pydantic import BaseSettings


class Settings(BaseSettings):
    TELEGRAM_TOKEN: str
    STEAM_TAX: float = 12.8309572
    PLATI_URL: str = "https://plati.market/itm/auto-steam-turkey-20-50-100-200-250-300-popoln-gift-kod/3403743"
    MARKET_URL: str = "https://market.csgo.com/"

    class Config:
        env_file = ".env"
        env_file_encoding = "utf-8"


settings = Settings()