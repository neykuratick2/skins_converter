# Простой бот для парсинга csgo market

### Prereqs:

1. Python 3.10 и выше: [Скачать](https://www.python.org/downloads/release/python-3100/)
2. Git: [Скачать](https://git-scm.com/downloads)
3. Бот созданный в [@botfather](https://t.me/botfather)
   - Инструкция по созданию: [Инструкция](https://botcreators.ru/blog/kak-sozdat-svoego-bota-v-botfather/)
4. Открыть любой терминал 
   - Windows: в поиске пишем `powershell` и нажимаем на синую консоль
   - Mac: нажимаем `Command + Пробел` и пишем `terminal`
   - Linux: ты сам знаешь как
   - FreeBSD: серьёзно?
   - HP-UX: 💀💀

### Как запустить:
Пишем эти комманды в терминале

1. `git clone https://gitlab.com/neykuratick2/skins_converter/`
2. `cd skins_converter`
3. `python -m venv venv`
4. `source venv/bin/activate`
5. `pip install -r requirements.txt`
6. Создаём файлик в этой же папке (skins_converter)
   - Название `.env`
   - Содержание: по примеру с `.env.exmaple` (Значения вставляем без символов `<` и `>`)
   - Если не знаешь какие вставлять значения, не вставляй, если это необязательное поле
7. Идём в бота и пишем ему названия скинов
8. `python main.py`

### Пример использования:

<img src="demo.png"  width="500" height="600" alt="demo.png">

### Описание полей кратко:

Если не хочется особо напрягаться, можно смотреть чисто только на одно поле - 
`1 Steam TRY = 4.67 RUB (с налогом стима)`.
Чем ниже - тем дешевле. На [Plati](https://plati.market/) всегда невыгодно покупать карты пополнения, по сравнению со скинами,
а курс банка я добавил просто для интереса


### Описание полей подробно:

- `Стомимость на маркете: 6598.00 RUB` Эта цифра показывает наименьшую стоимость, за которую этот скин можно купить на сайте https://market.csgo.com/
- `Стоимость в Steam: 1621.68 TRY` Эта цифра показывает наибольшую цену, которую можно выставить для покупателя на [стим маркете](https://steamcommunity.com/market/listings/730/), чтобы этот скин купили моментально (автопокупкой)
- `Стоимость в Steam: 1413.60 TRY (с налогом стима)` Эта цифра показывает, сколько в итоге придёт на счёт стима, если указать цену из предыдущей графы
- `Цена 1621.68 TRY` После этой строчки пойдут две другие, которые покажут, за сколько можно купить `1621.68` на разных площадках
- `На Plati: 8459.00 RUB` Столько бы пришлось заплатить [этому продавцу](https://plati.market/itm/auto-steam-turkey-20-50-100-200-250-300-popoln-gift-kod/3403743) с [плати маркета](https://plati.market/), чтобы получить `1621.68` лир на счёт стима
- `В банке: 6584.21 RUB` Такой бы эквивалент в рублях пришлось потратить, чтобы пополнить стим турецкой карточкой, на которой есть `1621.68` лир
- `Курсы валюты на разных сайтах:` После этой строчки идёт расчёт курса исходя из параметров сверху (чтобы было удобней сравнивать)
- `1 Steam TRY = 4.07 RUB` Такой курс ЛИРА к РУБЛЮ будет, если купить скин на https://market.csgo.com/ и продать его на [стим маркете](https://steamcommunity.com/market/listings/730/)
- `1 Steam TRY = 4.67 RUB (с налогом стима)` То же самое, только с учётом комиссии стима
- `1 Plati TRY = 5.22 RUB` Такой курс ЛИРА к РУБЛЮ будет, если накупить карточек пополнения у [этого продавца](https://plati.market/itm/auto-steam-turkey-20-50-100-200-250-300-popoln-gift-kod/3403743)
- `1 Plati TRY = 4.99 RUB (с налогом стима)` Не используется, не успел ещё убрать
- `1 Bank TRY = 4.06 RUB` Такой курс ЛИРА к РУБЛЮ будет, если конвертировать рубли в лиры по официальному курсу (без учёта комиссий банка)
- `1 Bank TRY = 4.06 RUB (с налогом стима)` Не используется, не успел ещё убрать


# Todo:

- [ ] Алгоритм поиска самой выгодной комбинации карточек на [Plati](https://plati.market/)
- [ ] Учитывать при вычислении курса лиры на [Plati](https://plati.market/) количество лир, которое ты получаешь за промокод (что 1 лира не стоит 78 рублей, это 10 лир)
- [ ] Быстрый и расширенный режим (В быстром режиме показывать только одну цифру - 1 Steam TRY с учётом налога)
- [ ] Вырезать `1 Plati TRY = 4.99 RUB (с налогом стима)`, `1 Bank TRY = 4.06 RUB (с налогом стима)`
- [ ] Добавить в `class Price` стринговую репрезентацию цены