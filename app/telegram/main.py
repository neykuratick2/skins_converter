import logging
import os
import traceback

from aiogram import Bot, Dispatcher, executor, types
from app.core.main import get_prices
from app.core.misc.proxy_manager import proxy_manager
from app.core.misc.supported_languages import CurrencyEnum
from app.telegram.item_template import convert_to_md
from config import settings

API_TOKEN = settings.TELEGRAM_TOKEN

# Configure logging
logging.basicConfig(level=logging.INFO)

# Initialize bot and dispatcher
bot = Bot(token=API_TOKEN)
dp = Dispatcher(bot)


@dp.message_handler(commands=["clear_proxy"])
async def send_welcome(message: types.Message):
    proxy_manager.clear_proxy()
    await message.answer(
        'Прокси очищены'
    )


@dp.message_handler(commands=["set_proxy"])
async def send_welcome(message: types.Message):
    split = message.text.split("/set_proxy ")
    if len(split) != 2 or not split[1]:
        return await message.answer(
            text='Error, usage: `/set_proxy user:password@ip:port`',
            parse_mode='Markdown',
        )

    proxy = split[1]
    proxy_manager.set_proxy(proxy)
    await message.answer(
        text=f'Установлено прокси {proxy_manager.get_proxy()}',
    )


@dp.message_handler(commands=["start", "help"])
async def send_welcome(message: types.Message):
    reply = 'Пришли мне название скина в формате "Название скина (Какое-состояние)"'
    await message.answer(reply)
    await message.answer(
        'Чтобы продать быстрее всего, в поле "Buyer pays" в стиме указывай '
        '"Стоимость в Steam" из результатов\n\n'
        ''
        'Тогда ты получишь "Стоимость в Steam: {твоя цена} (с налогом стима)"'
    )


@dp.message_handler()
async def echo(message: types.Message):

    checks = {
        'skin': [c not in message.text for c in ("(", ")", " ", "|")],
        'sticker': [c not in message.text for c in (" ", "|")]
    }

    if 'Sticker' in message.text:
        validators = checks.get('sticker')
    else:
        validators = checks.get("skin")

    if any(validators):
        return await message.answer(f"Неправильный формат")

    print(f"Searching for: {message.text}")
    try:
        result = await get_prices(message.text, currency=CurrencyEnum.TRY)
        result = convert_to_md(item=result)
    except Exception as e:
        traceback.print_exception(e)
        exception_text = str(e) or e.__class__
        result = f'Ошибка: {exception_text}'

    await message.answer(result, parse_mode="Markdown", disable_web_page_preview=True)


if __name__ == "__main__":
    executor.start_polling(dp, skip_updates=True)
