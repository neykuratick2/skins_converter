import os

from app.core.main import MarketPrice
from config import settings


def calculate_rates(item: MarketPrice):
    _steam_rate = round(1 / item.steam_price.amount * item.market_csgo_price.amount, 2)
    _steam_rate_taxed = round(1 / item.steam_price.amount_taxed * item.market_csgo_price.amount, 2)

    _plati_rate = round(item.plati_price.amount / item.steam_price.amount, 2)
    _plati_rate_taxed = round(item.plati_price.amount_taxed / item.steam_price.amount_taxed, 2)

    _bank_rate = round(item.exchange_rate_price.amount / item.steam_price.amount, 2)
    _bank_rate_taxed = round(item.exchange_rate_price.amount_taxed / item.steam_price.amount_taxed, 2)

    steam_rate = f"{_steam_rate:.2f} {item.market_csgo_price.currency}"
    steam_rate_taxed = f"{_steam_rate_taxed:.2f} {item.market_csgo_price.currency} (с налогом стима)"

    plati_rate = f"{_plati_rate:.2f} {item.market_csgo_price.currency}"
    plati_rate_taxed = f"{_plati_rate_taxed:.2f} {item.market_csgo_price.currency} (с налогом стима)"

    bank_rate = f"{_bank_rate:.2f} {item.market_csgo_price.currency}"
    bank_rate_taxed = f"{_bank_rate_taxed:.2f} {item.market_csgo_price.currency} (с налогом стима)"

    return (
        f"1 Steam {item.steam_price.currency} = {steam_rate}\n"
        f"1 Steam {item.steam_price.currency} = {steam_rate_taxed}\n\n"
        f"1 Plati {item.steam_price.currency} = {plati_rate}\n"
        f"1 Plati {item.steam_price.currency} = {plati_rate_taxed}\n\n"
        f"1 Bank {item.steam_price.currency} = {bank_rate}\n"
        f"1 Bank {item.steam_price.currency} = {bank_rate_taxed}\n\n"
    )


def convert_to_md(item: MarketPrice) -> str:
    market_url = settings.MARKET_URL
    market_price = f"{item.market_csgo_price.amount:.2f} {item.market_csgo_price.currency}"

    plati_url = settings.PLATI_URL
    plati = f"[Plati]({plati_url}): {item.plati_price.amount:.2f} {item.plati_price.currency}"
    plati_taxed = (
        f"[Plati]({plati_url}): {item.plati_price.amount_taxed:.2f} {item.plati_price.currency}"
    )

    steam_amount = f"{item.steam_price.amount:.2f} {item.steam_price.currency}"
    steam_amount_taxed = f"{item.steam_price.amount_taxed:.2f} {item.steam_price.currency}"

    steam_price = f"в [Steam]({item.url}): {steam_amount}"
    steam_taxed = f"в [Steam]({item.url}): {steam_amount_taxed} (с налогом стима)"

    rates = calculate_rates(item=item)

    result = (
        f"Стомимость на [маркете]({market_url}): {market_price}\n\n"
        f"Стоимость {steam_price}\n"
        f"Стоимость {steam_taxed}\n\n"
        f""
        f"Цена {steam_amount}\n\n"
        f"На {plati}\n"
        f"В банке: {item.exchange_rate_price.amount:.2f} {item.exchange_rate_price.currency}\n\n"
        f""
        f"Цена {steam_amount_taxed}\n\n"
        f"На {plati_taxed}\n"
        f"В банке: {item.exchange_rate_price.amount_taxed:.2f} {item.exchange_rate_price.currency}\n\n"
        f""
        f"Курсы валюты на разных сайтах: \n\n{rates}"
    )

    return result
