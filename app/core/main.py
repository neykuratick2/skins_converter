from typing import Optional

from app.core.apis import currency_exchange
from app.core.apis import market_csgo_com
from app.core.apis import plati_ru_api
from app.core.misc.supported_languages import CurrencyEnum
from app.core.apis.steam_api import get_highest_price
from pydantic import BaseModel, Field


class Price(BaseModel):
    amount: float
    amount_taxed: Optional[float]
    currency: str


class MarketPrice(BaseModel):
    url: str

    steam_price: Price = Field(
        ...,
        detail="How much can you get if you sell skin on steam market",
    )

    plati_price: Price = Field(
        ...,
        detail="How much rubles you must spend to get this much liras",
    )

    exchange_rate_price: Optional[Price] = Field(..., detail="Officially converted price")

    market_csgo_price: Price = Field(...)


async def get_prices(
    market_hash_name: str,
    currency: CurrencyEnum,
) -> MarketPrice:
    steam_price = await get_highest_price(
        market_hash_name=market_hash_name,
        currency=currency,
    )

    plati_price, plati_price_taxed = await plati_ru_api.get_price(
        price=steam_price.price,
        price_taxed=steam_price.price_taxed,
    )

    exchanged = await currency_exchange.convert_x_to_rub(
        amount=steam_price.price,
        cur=currency.name,
    )

    exchanged_taxed = await currency_exchange.convert_x_to_rub(
        amount=steam_price.price_taxed,
        cur=currency.name,
    )

    market_price, market_cur = await market_csgo_com.get_price(market_hash_name=market_hash_name)

    result = MarketPrice(
        url=steam_price.url,
        steam_price=Price(
            amount=steam_price.price,
            currency=currency.name,
            amount_taxed=steam_price.price_taxed,
        ),
        plati_price=Price(
            amount=plati_price,
            amount_taxed=plati_price_taxed,
            currency="RUB",
        ),
        exchange_rate_price=Price(
            amount=exchanged,
            amount_taxed=exchanged_taxed,
            currency="RUB",
        ),
        market_csgo_price=Price(
            amount=market_price,
            currency=market_cur,
        ),
    )

    return result
