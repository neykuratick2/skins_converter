import json

from config import settings
from httpx import AsyncClient


async def get_price(market_hash_name: str):
    url = "https://market.csgo.com/api/graphql"

    query = """
        query viewItem($id: String, $market_hash_name: String!, $phase: String) {
            viewItem(id: $id, market_hash_name: $market_hash_name, phase: $phase) {
                price
                currency
            }
        }
    """

    variables = {"market_hash_name": market_hash_name}

    payload = {"operationName": "viewItem", "variables": variables, "query": query}

    headers = {
        "authority": "market.csgo.com",
        "accept": "application/json, text/plain, */*",
        "accept-language": "en-GB,en-US;q=0.9,en;q=0.8,ru;q=0.7",
        "app-lang": "ru",
        "content-type": "application/json",
        "origin": "https://market.csgo.com",
        "referer": "https://market.csgo.com/en/Rifle/M4A4/M4A4%20%7C%20X-Ray%20%28Factory%20New%29",
        "sec-ch-ua": '"Google Chrome";v="111", "Not(A:Brand";v="8", "Chromium";v="111"',
        "sec-ch-ua-mobile": "?0",
        "sec-ch-ua-platform": '"macOS"',
        "sec-fetch-dest": "empty",
        "sec-fetch-mode": "cors",
        "sec-fetch-site": "same-origin",
        "user-agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, "
        "like Gecko) Chrome/111.0.0.0 Safari/537.36",
    }

    async with AsyncClient() as client:
        response = await client.post(url, headers=headers, json=payload)
    
    try:
        data = response.json()["data"]["viewItem"]
    except Exception as e:
        print(f"\n\n{response.text=}\n\n")

        if 'techworks' in response.text:
            raise ValueError(
                f"Не получилось загрузить данные с {settings.MARKET_URL}, "
                f"т.к там проводятся технические работы"
            )

        raise e

    price = round(float(data["price"]), 2)
    currency = data["currency"]

    return price, currency
