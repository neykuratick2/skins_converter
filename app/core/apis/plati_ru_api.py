import collections
import os

import httpx
from bs4 import BeautifulSoup
from config import settings


async def __get_prices() -> collections.OrderedDict[int, int]:
    url = settings.PLATI_URL
    async with httpx.AsyncClient() as client:
        response = await client.get(url)

    soup = BeautifulSoup(response.text, "html.parser")

    prices = {}

    labels = soup.select_one('div[class="sort_by goods_pay_by_select clearfix"]').select("label")
    base, base_key = None, None

    for label in labels:
        if label is None:
            continue

        liras = int(label.text.split(" TL")[0])

        rub_raw = label.select_one('span[style="font-size:11px;color:#A0A0A0;"]')
        if not rub_raw:
            prices[liras] = 0
            input_ = label.select_one("input")
            base = (
                f'<response><option O="{input_["data-item-id"]}" V="'
                f'{input_["value"]}"/></response>'
            )
            base_key = liras
            continue

        rub = int(rub_raw.text.split(" Ru")[0].split("(+")[1])
        prices[liras] = rub

    if not base or not base_key:
        raise ValueError(f"{base=}, {base_key=}")

    params = {
        "p": "3403743",
        "n": "0",
        "c": "RUB",
        "e": "",
        "d": "true",
        "x": base,
        "rnd": "0.7109541874344154",
    }

    async with httpx.AsyncClient() as client:
        response = await client.get(
            url="https://plati.market/asp/price_options.asp",
            params=params,
        )

    base_price = int(response.json()["price"])
    for key, value in prices.items():
        prices[key] = value + base_price

    od = collections.OrderedDict(sorted(prices.items()))
    return od


def __find_lest_expensive(prices: dict[int, int], target: int | float):
    # TODO optimize

    result = 0
    biggest_lesser_used = False
    for i in range(2):
        biggest_lesser = next(iter(prices.keys()))
        smallest_bigger = -1
        for lira in prices.keys():
            if lira < target:
                biggest_lesser = lira
            else:
                smallest_bigger = lira
                break

        if target - biggest_lesser <= 0:
            result = prices[biggest_lesser]
            break
        elif target - smallest_bigger <= 0:
            result = prices[smallest_bigger]
            break

        target -= biggest_lesser

        if not biggest_lesser_used:
            result += prices[biggest_lesser]
            biggest_lesser_used = True
        else:
            result += prices[smallest_bigger]

    return result


async def get_price(price: int | float, price_taxed: int | float) -> tuple[int, int]:
    prices = await __get_prices()
    lest_expensive = __find_lest_expensive(prices=prices, target=price)
    lest_expensive_taxed = __find_lest_expensive(prices=prices, target=price_taxed)
    return lest_expensive, lest_expensive_taxed
