import os
import urllib.parse

from app.core.misc.proxy_manager import proxy_manager
from config import settings
from pydantic import BaseModel
from httpx import AsyncClient, Timeout

from app.core.misc.supported_languages import CurrencyEnum


class GetHighestPriceOut(BaseModel):
    url: str
    price: float
    price_taxed: float


def calculate_steam_tax(number: float) -> float:
    tax_rate = settings.STEAM_TAX
    result = number - (number * tax_rate / 100)

    return round(result, 2)


async def get_highest_price(market_hash_name: str, currency: CurrencyEnum) -> GetHighestPriceOut:
    # return GetHighestPriceOut(url="https://store.steampowered.com/", price=7, price_taxed=6)

    market_url = f"https://steamcommunity.com/market/listings/730/{market_hash_name}"
    encoded_url = urllib.parse.quote(market_url, safe=":/")

    proxies = proxy_manager.get_proxy()

    async with AsyncClient(proxies=proxies) as client:
        # timeout = Timeout(connect=5, read=None, write=None, pool=None)
        response = await client.get(encoded_url)

    if "There are no listings for this item." in response.text:
        raise ValueError(f"Item not found")

    item_nameid = response.text.split("function() { Market_LoadOrderSpread( ")[1].split(" ); }")[0]

    params = {
        "country": "TR",
        "language": "english",
        "currency": currency.value,
        "item_nameid": item_nameid,
        "two_factor": 0,
    }

    async with AsyncClient() as client:
        listings_response = await client.get(
            url="https://steamcommunity.com/market/itemordershistogram", params=params
        )

    if listings_response.status_code == 429:
        raise ValueError(
            "Steam api sent 429 - too many requests \n\n"
            "Можно установить прокси для этого бота, чтобы он отправлял запросы с другого айпи: "
            "/set\\_proxy"
            # ""
        )

    listings = listings_response.json()
    price = round(float(listings.get("highest_buy_order", 0)) / 100, 2)
    price_taxed = calculate_steam_tax(number=price)
    return GetHighestPriceOut(url=encoded_url, price=price, price_taxed=price_taxed)
