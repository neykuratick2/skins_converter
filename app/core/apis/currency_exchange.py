from httpx import AsyncClient


async def convert_x_to_rub(amount: int | float, cur: str) -> float:
    url = (
        f"https://raw.githubusercontent.com/fawazahmed0/currency-api/1/latest/currencies/"
        f"{cur.lower()}/rub.json"
    )

    async with AsyncClient() as client:
        r = await client.get(url=url)

    rub = float(r.json()["rub"])
    return round(amount * rub, 2)
