from typing import Optional


class ProxyManager:
    def __init__(self):
        self._proxy: dict = {}

    def set_proxy(self, proxy: str):
        self._proxy = {
            "all://": f"http://{proxy}",
        }

    def clear_proxy(self):
        self._proxy = None

    def get_proxy(self):
        return self._proxy.get("all://")


proxy_manager = ProxyManager()
